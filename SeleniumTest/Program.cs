﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Net;

namespace SeleniumTest
{

    class Program
    {
        static void Main(string[] args)
        {
        }

        [SetUp]
        public void Initialize()
        {

            string Url = "https://kilofy.000webhostapp.com/";
            //string Url = "http://localhost/";

            PropertiesCollection.Driver = new FirefoxDriver();
            PropertiesCollection.Driver.Navigate().GoToUrl(Url);

            Console.WriteLine("Open URL");
        }

        [Test]
        public void CanLogIn()
        {
            LoginPageObject pageLogin = new LoginPageObject();
            EAPageObject pageEA = pageLogin.Login("Darek", "123456");

            pageEA.FillUserForm("Darek", "Project K");
        }

        [Test]
        public void CanLogOut()
        {
            LoginPageObject pageLogin = new LoginPageObject();
            EAPageObject pageEA = pageLogin.Login("Darek", "123456");

            pageLogin = pageEA.LogOut();
            Assert.IsFalse(pageEA.IsLoggedIn());

        }

        [Test]
        public void CanGoToHomePage()
        {
            // TODO: refactor
            LoginPageObject pageLogin = new LoginPageObject();      
            Assert.IsTrue(pageLogin.IsAt());
        }

        [Test]
        public void CanRegisterNewAccount()
        {

        }

        [Test]
        public void CanClickAllLinksOnPage()
        {
            LoginPageObject pageLogin = new LoginPageObject();
            pageLogin.CheckStatusATagLinks();

            Assert.That(() => pageLogin.CheckStatusATagLinks(), Throws.Nothing);
        }

        [Test]
        public void CanClickAllImageLinksOnPage()
        {
            LoginPageObject pageLogin = new LoginPageObject();
            pageLogin.CheckStatusImgLinks();

            Assert.That(() => pageLogin.CheckStatusImgLinks(), Throws.Nothing);
        }

        [TearDown]
        public void Cleanup()
        {
            try
            {
                PropertiesCollection.Driver.Close();
                Console.WriteLine("Close the browser");
            }
            catch
            {
                //Ignore errors if unable to Close the browser
            }
        }
    }
}
