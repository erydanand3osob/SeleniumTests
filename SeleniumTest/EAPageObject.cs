﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTest
{
    class EAPageObject
    {
        public EAPageObject()
        {
            PageFactory.InitElements(PropertiesCollection.Driver, this);
        }

        [FindsBy(How = How.Name, Using = "username")]
        public IWebElement TxtName { get; set; }

        [FindsBy(How = How.Name, Using = "text")]
        public IWebElement TxtText { get; set; }

        [FindsBy(How = How.Id, Using = "nameId")]
        public IWebElement TdlNameId { get; set; }

        [FindsBy(How = How.Name, Using = "submitorder")]
        public IWebElement Btnsubmit { get; set; }

        [FindsBy(How = How.Name, Using = "logout")]
        private IWebElement logOutLink;

        public void FillUserForm(string userName, string text)
        {
            SeleniumSetMethod.EnterText(TxtName, userName);
            SeleniumSetMethod.EnterText(TxtText, text);
            SeleniumSetMethod.Clicks(Btnsubmit);

            TxtName.EnterText(userName);
            TxtText.EnterText(text);
        }

        public LoginPageObject LogOut()
        {
            if (logOutLink.Exists())
                logOutLink.Click();
            return new LoginPageObject();
        }

        public bool IsLoggedIn()
        {
            return logOutLink.Exists();         
        }
    }
}
