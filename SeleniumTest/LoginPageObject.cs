﻿namespace SeleniumTest
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Security.Policy;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    class LoginPageObject
    {
        public LoginPageObject()
        {
            PageFactory.InitElements(PropertiesCollection.Driver, this);
        }

        [FindsBy(How = How.Name, Using = "uid")]
        public IWebElement TxtUserName { get; set; }

        [FindsBy(How = How.Name, Using = "pwd")]
        public IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.Name, Using = "submit")]
        public IWebElement BtnLogin { get; set; }

        [FindsBy(How = How.TagName, Using = "a")]
        public IList<IWebElement> aLinks { get; set; }

        [FindsBy(How = How.TagName, Using = "img")]
        public IList<IWebElement> ImageLinks { get; set; }

        public EAPageObject Login(string userName, string password)
        {
            TxtUserName.EnterText(userName);
            TxtPassword.EnterText(password);
            BtnLogin.Clicks();

            return new EAPageObject();
        }

        public bool IsAt()
        {
            return PropertiesCollection.Driver.Title.Contains("Home");
        }
        //TODO: console write change to write logs

        public void CheckStatusATagLinks()
        {
            Console.WriteLine("Total number of elements found : " + aLinks.Count);

            foreach (IWebElement Link in aLinks)
            {
                string urlLink = Link.GetAttribute("href");

                if (!(Link == null))
                {
                    try
                    {
                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlLink);
                        webRequest.AllowAutoRedirect = false;
                        HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                        if (response.StatusCode == HttpStatusCode.OK)
                            Console.WriteLine($"URL: {urlLink} status is : 200 {response.StatusCode}");

                        response.Close();

                    }
                    catch (WebException e)
                    {

                        HttpWebResponse errorResponse = e.Response as HttpWebResponse;

                        if (errorResponse.StatusCode == HttpStatusCode.NotFound)
                        {
                            Console.WriteLine($"URL: {urlLink} status is : 404 - not found");
                        }
                        else
                        {
                            Console.WriteLine($"URL: {urlLink} status is : {0}", errorResponse.StatusCode.ToString());
                        }

                        throw new Exception("Invalid Status Code... ", e);
                    }
                }
            }
        }
        public void CheckStatusImgLinks()
        {
            // TODO: check links src
            // TODO: get image with javascript
            Console.WriteLine("Total number of images elements found : " + ImageLinks.Count);

            //Console.WriteLine(ImageLinks);
            foreach (IWebElement LinkSrc in ImageLinks)
            {
                Thread.Sleep(5000);
                
                string imageSrc = LinkSrc.GetAttribute("src");
                
               // Console.WriteLine(imageSrc.ToString());


                if (!(LinkSrc == null))
                {
                    try
                    {
                        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(imageSrc);
                        webRequest.AllowAutoRedirect = false;
                        HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                        if (response.StatusCode == HttpStatusCode.OK)
                            Console.WriteLine($"URL: {imageSrc} status is : 200 {response.StatusCode}");

                        response.Close();
                    }
                    catch (WebException e)
                    {
                        HttpWebResponse errorResponse = e.Response as HttpWebResponse;

                        if (errorResponse.StatusCode == HttpStatusCode.NotFound)
                        {
                            Console.WriteLine($"URL: {imageSrc} status is : 404 - not found");
                        }
                        else
                        {
                            Console.WriteLine($"URL: {imageSrc} status is : {0}", errorResponse.StatusCode.ToString());
                        }
                        throw new Exception("Invalid Status Code... ", e);
                    }
                }
            }
        }   
    }
}
